from typing import Dict
from benedict import benedict


def load_config(stage: str) -> Dict:
    try:
        common_config = benedict.from_yaml('config/common.yaml')
    except ValueError:
        print('No config found in config/common.yaml.')
        common_config = benedict([])

    try:
        env_config = benedict.from_yaml('config/' + stage + '.yaml')
    except ValueError:
        print('No config found in config/' + stage + '.yaml')
        env_config = benedict([])

    common_config.merge(env_config)

    common_config['stage'] = stage

    return common_config
