from botocore.vendored import requests
import boto3
import os

URL = os.environ["ENDPOINT"]
SNS_TOPIC = os.environ["SNS_TOPIC"]
PROTOCOL = os.getenv("PROTOCOL", 443)


def handler(event, context):

    isWebsiteUp = checkWebsite(URL)

    if not isWebsiteUp:
        print("sending alert")
        sns_client = boto3.client("sns")
        sns_client.publish(
            TopicArn=SNS_TOPIC,
            Subject="{} is not reachable".format(URL),
            Message="{} is down".format(URL),
        )

    return isWebsiteUp


def checkWebsite(target_url):
    if PROTOCOL == 443:
        url = "{}://{}".format("https", target_url)
    elif PROTOCOL == 80:
        url = "{}://{}".format("http", target_url)
    else:
        url = "{}://{}:{}".format("http", target_url, PROTOCOL)
    try:
        response = requests.get(url)
        if response.ok:
            return True
        return False
    except requests.exceptions.ConnectionError:
        return False
