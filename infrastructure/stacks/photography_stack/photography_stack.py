from aws_cdk import (
    Stack,
    CfnOutput,
    aws_lambda as _lambda,
    aws_s3 as s3,
    aws_lambda_event_sources as lambda_sources,
    aws_events as events,
    aws_dynamodb as dynamodb,
    aws_sns as sns,
    aws_sns_subscriptions as sns_subs,
    aws_events_targets as targets,
    aws_cloudfront as cloudfront,
    aws_cloudfront_origins as origins,
    aws_apigateway as apigateway,
)
from constructs import Construct
from typing import Dict


class PhotogtaphyStack(Stack):
    def __init__(
        self, scope: Construct, construct_id: str, config: Dict, **kwargs
    ) -> None:
        super().__init__(scope, construct_id)

        # Note: Lifecycle Rules are optional but are included here to keep costs
        #       low by cleaning up old files or moving them to lower cost storage options
        appBucket = s3.Bucket(
            self,
            id="{}-app-bucket-{}".format(config["projectName"],
                                         config["environment"]),
            bucket_name="{}-app-bucket-{}".format(
                config["projectName"], config["environment"]
            ),
            block_public_access=s3.BlockPublicAccess.BLOCK_ALL,
            versioned=False,
        )

        photoBucket = s3.Bucket(
            self,
            id="{}-photo-bucket-{}".format(
                config["projectName"], config["environment"]
            ),
            bucket_name="{}-photo-bucket-{}".format(
                config["projectName"], config["environment"]
            ),
            block_public_access=s3.BlockPublicAccess.BLOCK_ALL,
            versioned=False,
        )

        appDistribution = cloudfront.Distribution(
            self,
            id="{}-cloudfront-{}".format(config["projectName"],
                                         config["environment"]),
            default_behavior=cloudfront.BehaviorOptions(
                origin=origins.S3Origin(appBucket)
            ),
            default_root_object="index.html",
            additional_behaviors={
                "/photos/*": cloudfront.BehaviorOptions(
                    origin=origins.S3Origin(photoBucket)
                )
            },
        )

        createFunction = _lambda.Function(
            self,
            "{}-upload-function-{}".format(
                config["projectName"], config["environment"]
            ),
            handler="index.handler",
            runtime=_lambda.Runtime.NODEJS_14_X,
            code=_lambda.Code.from_asset(
                "stacks/photography_stack/lambda/create"),
            environment={"BUCKET": photoBucket.bucket_name},
        )

        photoBucket.grant_write(createFunction)

        cors_options = apigateway.CorsOptions(
            allow_origins=apigateway.Cors.ALL_ORIGINS,
            allow_methods=apigateway.Cors.ALL_METHODS,
        )

        photoApi = apigateway.RestApi(
            self,
            id="{}-apigw-{}".format(config["projectName"],
                                    config["environment"]),
            rest_api_name="{}-apigw-{}".format(
                config["projectName"], config["environment"]
            ),
            default_cors_preflight_options=cors_options,
        )
        # const uploadResource = photoApi.root.addResource("photo");
        uploadResource = photoApi.root.add_resource("photo")
        uploadResource.add_method(
            "POST", apigateway.LambdaIntegration(createFunction))

        # HERE 1

        photoTable = dynamodb.Table(
            self,
            id="{}-dynamodb-{}".format(config["projectName"],
                                       config["environment"]),
            partition_key=dynamodb.Attribute(
                name="id", type=dynamodb.AttributeType.STRING
            ),
        )

        registerFunction = _lambda.Function(
            self,
            "{}-register-function-{}".format(
                config["projectName"], config["environment"]
            ),
            handler="index.handler",
            runtime=_lambda.Runtime.NODEJS_14_X,
            code=_lambda.Code.from_asset(
                "stacks/photography_stack/lambda/register"),
            environment={"TABLE_NAME": photoTable.table_name},
        )

        registerFunction.add_event_source(
            lambda_sources.S3EventSource(
                photoBucket, events=[s3.EventType.OBJECT_CREATED]
            )
        )

        photoBucket.grant_read(registerFunction)
        photoTable.grant_write_data(registerFunction)

        listFunction = _lambda.Function(
            self,
            "{}-list-function-{}".format(config["projectName"],
                                         config["environment"]),
            handler="index.handler",
            runtime=_lambda.Runtime.NODEJS_14_X,
            code=_lambda.Code.from_asset(
                "stacks/photography_stack/lambda/list"),
            environment={"TABLE_NAME": photoTable.table_name},
        )

        photoTable.grant_read_data(listFunction)
        uploadResource.add_method(
            "GET", apigateway.LambdaIntegration(listFunction))

        deleteFunction = _lambda.Function(
            self,
            "{}-delete-function-{}".format(
                config["projectName"], config["environment"]
            ),
            handler="index.handler",
            runtime=_lambda.Runtime.NODEJS_14_X,
            code=_lambda.Code.from_asset(
                "stacks/photography_stack/lambda/delete"),
            environment={
                "TABLE_NAME": photoTable.table_name,
                "BUCKET_NAME": photoBucket.bucket_name,
            },
        )

        photoTable.grant_write_data(deleteFunction)
        photoBucket.grant_delete(deleteFunction)
        uploadResource.add_method(
            "DELETE", apigateway.LambdaIntegration(deleteFunction)
        )

        monitorTopic = sns.Topic(
            self,
            id="{}-sns-topic-{}".format(config["projectName"],
                                        config["environment"]),
            display_name="{}-sns-topic-{}".format(
                config["projectName"], config["environment"]
            ),
            topic_name="WebsiteIsDown-{}".format(config["environment"]),
        )

        monitorWebsiteFunction = _lambda.Function(
            self,
            "{}-monitor-function-{}".format(
                config["projectName"], config["environment"]
            ),
            handler="ping.handler",
            runtime=_lambda.Runtime.PYTHON_3_7,
            code=_lambda.Code.from_asset(
                "stacks/photography_stack/lambda/monitor"),
            environment={
                "SNS_TOPIC": monitorTopic.topic_arn,
                "ENDPOINT": appDistribution.domain_name,
            },
        )

        monitorTopic.grant_publish(monitorWebsiteFunction)
        monitorTopic.add_subscription(
            sns_subs.EmailSubscription("vovxox1@gmail.com"))

        healthCheckRule = events.Rule(
            self,
            "JSONRule",
            schedule=events.Schedule.cron(
                minute="*", hour="*", month="*", week_day="*", year="*"
            ),
        )

        healthCheckRule.add_target(
            targets.LambdaFunction(
                monitorWebsiteFunction, event=events.RuleTargetInput.from_object({
                })
            )
        )

        CfnOutput(self, "AppBucket", value=appBucket.bucket_name)
        # CfnOutput(self, "CfDistDomainName", value=appDistribution.)
