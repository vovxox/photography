#!/usr/bin/env python3
import sys
import aws_cdk as cdk
from utils import config_util
from stacks.photography_stack.photography_stack import PhotogtaphyStack

app = cdk.App()

stage = app.node.try_get_context('stage')
if stage is None or stage == "unknown":
    sys.exit('You need to set the target stage.'
             ' USAGE: cdk <command> -c stage=dev <stack>')

# Load stage config and set cdk environment
config = config_util.load_config(stage)

PhotogtaphyStack(
    app, "photoStack-{}".format(config["environment"]), config=config)

app.synth()
